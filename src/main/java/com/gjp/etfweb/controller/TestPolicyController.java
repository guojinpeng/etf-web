package com.gjp.etfweb.controller;

import com.gjp.etfweb.abstracts.AbstractPolicy;
import com.gjp.etfweb.bean.FundInfo;
import com.gjp.etfweb.business.CommonBusiness;
import com.gjp.etfweb.common.Constant;
import com.gjp.etfweb.form.QueryForm;
import com.gjp.etfweb.policys.FixedInvestmentPolicy;
import com.gjp.etfweb.utils.SpringUtils;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.math.BigDecimal;
import java.util.List;

@RestController
public class TestPolicyController {
    private final CommonBusiness commonBusiness;

    public TestPolicyController(CommonBusiness commonBusiness) {
        this.commonBusiness = commonBusiness;
    }

    @GetMapping("/policy/{policyName}")
    public ModelAndView testPolicy(QueryForm form, @PathVariable String policyName) {
        AbstractPolicy policy;
        try {
            policy = (AbstractPolicy) SpringUtils.getBean(policyName);
        } catch (Exception e) {
            policy = new FixedInvestmentPolicy();
        }

        ModelAndView modelAndView = new ModelAndView("testPolicy");
        FundInfo fundInfo = commonBusiness.processing(form, Constant.FILE_ROOT, policy);
        modelAndView.addObject("fundInfo", fundInfo);
        // 测试其他收益率
        List<FundInfo> FundInfoTestList = test(form, policy);
        modelAndView.addObject("fundInfoTest", FundInfoTestList);
        return modelAndView;
    }

    private List<FundInfo> test(QueryForm form, AbstractPolicy policy) {
        form.setIsNew(false);
        // 测试年化收益率范围
        BigDecimal startPoint = BigDecimal.valueOf(0.05);
        BigDecimal endPoint = BigDecimal.valueOf(1);
        // 步长
        BigDecimal step = BigDecimal.valueOf(0.01);
        // 测试收益率
        List<FundInfo> FundInfoTestList = Lists.newArrayList();
        for (BigDecimal stopPoint = startPoint; stopPoint.compareTo(endPoint) < 0; ) {
            form.setStopPoint(stopPoint);
            FundInfo fundInfoTest = commonBusiness.processing(form, Constant.FILE_ROOT, policy);
            FundInfoTestList.add(fundInfoTest);
            stopPoint = stopPoint.add(step);
        }
        return FundInfoTestList;
    }
}
