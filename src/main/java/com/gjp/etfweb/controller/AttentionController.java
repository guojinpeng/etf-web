package com.gjp.etfweb.controller;

import com.gjp.etfweb.bean.FundInfo;
import com.gjp.etfweb.business.CommonBusiness;
import com.gjp.etfweb.common.Constant;
import com.gjp.etfweb.form.QueryForm;
import com.gjp.etfweb.policys.MoreFallMoreBuyPolicy;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController
public class AttentionController {
    private final CommonBusiness commonBusiness;

    public AttentionController(CommonBusiness commonBusiness) {
        this.commonBusiness = commonBusiness;
    }

    @GetMapping("/")
    public ModelAndView testPolicy(String codeList, String starCodeList) {
        ModelAndView modelAndView = new ModelAndView("main");
        modelAndView.addObject("fundInfoList", testPolicy(codeList));
        modelAndView.addObject("starFundInfoList", testPolicy(starCodeList));
        modelAndView.addObject("codeList", codeList);
        modelAndView.addObject("starCodeList", starCodeList);
        return modelAndView;
    }

    public List<FundInfo> testPolicy(String codeList) {
        List<FundInfo> fundInfoList = Lists.newArrayList();
        if (!StringUtils.isEmpty(codeList)) {
            for (String code : codeList.split(",")) {
                QueryForm form = new QueryForm();
                form.setCode(code);
                FundInfo fundInfo = commonBusiness.processing(form, Constant.FILE_ROOT, new MoreFallMoreBuyPolicy());
                fundInfoList.add(fundInfo);
            }
        }
        return fundInfoList;
    }
}
