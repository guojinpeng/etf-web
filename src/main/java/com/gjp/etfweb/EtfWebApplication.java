package com.gjp.etfweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EtfWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(EtfWebApplication.class, args);
    }

}
