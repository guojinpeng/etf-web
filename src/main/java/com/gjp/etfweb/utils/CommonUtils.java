package com.gjp.etfweb.utils;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CommonUtils {

    public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    public static DecimalFormat percentFormat = new DecimalFormat("0.0000%");
    public static DecimalFormat numberFormat = new DecimalFormat("0.0000");

    public static Date parseDate(String dateStr) {
        try {
            return dateFormat.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Date parseDate(String dateStr, String patten) {
        SimpleDateFormat sdf = new SimpleDateFormat(patten);
        try {
            return sdf.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String dateToString(Date date) {
        return dateFormat.format(date);
    }

    public static Integer intervalDays(Date startTime, Date endTime) {
        // 获取相差的天数
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startTime);
        Long startTimeInMillis = calendar.getTimeInMillis();
        calendar.setTime(endTime);
        Long endTimeInMillis = calendar.getTimeInMillis();
        Long betweenDays = (endTimeInMillis - startTimeInMillis) / (1000L * 3600L * 24L);
        return betweenDays.intValue();
    }
}
