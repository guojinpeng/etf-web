package com.gjp.etfweb.bean;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 测试结果分析对象
 */
@Data
public class FundTestResult {
    // 起始时间
//    private Date startTime;
    // 结束时间
    private Date endTime;
    // 收益率
    private BigDecimal earningsRate;
    // 年化收益率
    private BigDecimal yearEarningsRate;
    // 总成本
    public BigDecimal totalCost;
    // 总利润
    public BigDecimal totalProfit;
}
