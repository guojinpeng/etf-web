package com.gjp.etfweb.bean;

import java.util.HashMap;

/**
 * 通用返回值对象
 *
 * @author 郭金鹏
 */
public class Result extends HashMap<String, Object> {

    public Result() {
        setCode(200);
        setMsg("");
        setData("");
    }

    public Result(Integer code, String msg, Object data) {
        setCode(code);
        setMsg(msg);
        setData(data);
    }

    public static Result error(String msg) {
        return error(msg, "");
    }

    public static Result error(Integer code, String msg) {
        return error(code, msg, "");
    }

    public static Result error(String msg, Object data) {
        return error(500, msg, data);
    }

    private static Result error(Integer code, String msg, Object data) {
        return new Result(code, msg, data);
    }

    public static Result ok() {
        return ok("");
    }

    public static Result ok(Object data) {
        return ok("", data);
    }

    public static Result ok(String msg, Object data) {
        return ok(200, msg, data);
    }

    private static Result ok(Integer code, String msg, Object data) {
        return new Result(code, msg, data);
    }

    @Override
    public Result put(String key, Object value) {
        super.put(key, value);
        return this;
    }

    public Result setData(Object data) {
        super.put("data", data);
        return this;
    }

    public Result setMsg(Object msg) {
        super.put("msg", msg);
        return this;
    }

    public Result setCode(Object code) {
        super.put("code", code);
        return this;
    }
}
