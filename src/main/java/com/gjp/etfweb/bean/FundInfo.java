package com.gjp.etfweb.bean;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gjp.etfweb.form.QueryForm;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * 基金相关详情
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FundInfo {
    // 平均成本
    public String name;
    // 平均成本
    public BigDecimal averageCost;
    // 累计定投金额
    public BigDecimal totalBuyAmount;
    // 累计定投份额
    public BigDecimal totalShare;
    // 总投入天数
    public BigDecimal totalDay;
    // 定投收益
    public BigDecimal earningsAmount;
    // 定投收益率
    public BigDecimal earningsRate;
    // 年化收益率
    public BigDecimal yearEarningsRate;
    // 总止盈次数
    public Integer stopNum;
    // 总成本
    public BigDecimal totalCost;
    // 总利润
    public BigDecimal totalProfit;
    // 累计间隔天数
    public BigDecimal totalDayInterval;
    // 连续跌幅
    public BigDecimal continuousDecline;

    // 入参数据
    public QueryForm queryParam;

    //定投详情
    private List<FundDetails> detailsList;
}
