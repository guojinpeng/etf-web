package com.gjp.etfweb.form;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class FundPortfolioRootForm {
    /**
     * 总购买金额
     */
    BigDecimal initBuyAmount = BigDecimal.valueOf(100);
    /**
     * 每隔多少天定投一次：日定投填1，周定投填5，月定投填21.75
     */
    BigDecimal dayInterval = BigDecimal.valueOf(1);
    /**
     * 基金组合
     */
    private List<FundPortfolioForm> fundList;
}
